package id.co.indivara.db;
import com.mysql.jdbc.Connection;
import java.sql.*;

public class Database {
    private final String USERNAME = "root";
    private final String PASSWORD = "";
    private final String DATABASE = "hr";
    private final String HOST = "localhost:3306";
    private final String CONNECTION = String.format("jdbc:mysql://%s/%s", HOST, DATABASE);

    private Connection con;
    private Statement stmt;

    public Database() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con = (Connection) DriverManager.getConnection(CONNECTION, USERNAME, PASSWORD);
            stmt = con.createStatement();
            System.out.println("database connected");

        }catch (Exception e){
            System.out.println("database ");
            System.out.println(e);
        }
    }

    public ResultSet query(String query) throws SQLException {
        return stmt.executeQuery(query);
    }

    public void queryLogger(ResultSet q) {
        try {
            ResultSetMetaData md = q.getMetaData();
            int mdcol = md.getColumnCount();

            while(q.next()){
                for(int i=1; i <= mdcol; i++){
                    System.out.println(md.getColumnName(i) + ": " + q.getString(md.getColumnName(i)));
                }
                System.out.println("\n");
            }

        }catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        con.close();
        System.out.println("database closed");
    }

}
